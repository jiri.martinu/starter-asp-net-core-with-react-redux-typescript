# ASP.NET Core + React/Redux/Typescript template

## What is in it?

- Dotnet Core 2.1
- React + Redux
- Typescript
- SSR (via snapshoting + custom controller to serve prerendered files)
- React Hot Reloader (via react-app-rewire-hot-loader)

## Installation 

    git clone git@gitlab.com:jiri.martinu/starter-asp-net-core-with-react-redux-typescript.git
    dotnet new -i starter-asp-net-core-with-react-redux-typescript

## Usage 

    dotnet new reactreduxtypescript -n 'name-of-application'