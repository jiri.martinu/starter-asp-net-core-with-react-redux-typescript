import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import FetchData from './FetchData';
import Counter from './Counter';

export default class Main extends React.Component<{}, {}> {
    public render() {
        return <main>
            <Switch>
                <Route exact={true} path='/' component={ Home } />
                <Route path='/counter' component={ Counter } />
                <Route path='/fetchdata/:startDateIndex?' component={ FetchData } />
            </Switch>
      </main>
    }
}