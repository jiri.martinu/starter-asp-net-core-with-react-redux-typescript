import * as React from 'react';
import { hot } from 'react-hot-loader';
import Main from './components/Main';
import { Grid, Row, Col } from 'react-bootstrap';
import { NavMenu } from './components/NavMenu';

class App extends React.Component<{}, {}> {
  public render() {
    return <Grid fluid={true}>
        <Row>
            <Col sm={2}>
                <NavMenu />
            </Col>
            <Col sm={10}>
                <Main />
            </Col>
        </Row>
    </Grid>
  }
}

export default hot(module)(App);
